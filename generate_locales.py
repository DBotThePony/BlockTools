
# 
# Copyright (C) 2017 DBot
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#      http://www.apache.org/licenses/LICENSE-2.0
#  
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 

locales = [
	['iron', 'Iron'],
	['gold', 'Golden'],
	['diamond', 'Diamond'],
	['emerald', 'Emerald'],
	['copper', 'Copper'],
	['zinc', 'Zinc'],
	['lead', 'Lead'],
	['tin', 'Tin'],
	['silver', 'Silver'],
	['platinum', 'Platinum'],
	['bronze', 'Bronze'],
	['invar', 'Invar'],
	['brass', 'Brass'],
	['steel', 'Steel'],
]

outputFile = open('src/main/resources/assets/blocktools/lang/en_us.lang', 'w')

for localeData in locales:
	outputFile.write('\n')
	for st in ['axe', 'pickaxe', 'shovel', 'sword', 'hoe', 'boots', 'leggings', 'chestplate', 'helmet']:
		outputFile.write('item.blocktools.{internal}_{tool}.name={name} Block {name_upper}\n'.format(
			internal = localeData[0],
			tool = st,
			name = localeData[1],
			name_upper = st[0].upper() + st[1:]
		))

outputFile.close()
