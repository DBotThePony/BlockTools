
# 
# Copyright (C) 2017 DBot
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#      http://www.apache.org/licenses/LICENSE-2.0
#  
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 

# IMAGE MAGICK IS REQUIRED!

import subprocess

assets = [
	['iron', 'White'],
	['gold', 'Gold'],
	['diamond', '#6495ED'],
	['emerald', 'Lime'],
	['copper', 'rgb(184,115,51)'],
	['zinc', '#778899'],
	['lead', '#D0D0D0'],
	['tin', '#D3D3D3'],
	['silver', '#C0C0C0'],
	['platinum', '#B0C4DE'],
	['bronze', '#CD853F'],
	['invar', '#F5DEB3'],
	['brass', '#808080'],
	['steel', '#808080'],
]

for assetData in assets:
	for st in ['axe', 'pickaxe', 'shovel', 'sword', 'hoe']:
		jsonFile = '{{"parent": "item/handheld", "textures": {{"layer0": "blocktools:items/{type}_{item}"}}}}'.format(type = assetData[0], item = st)
		fl = open('src/main/resources/assets/blocktools/models/item/{type}_{item}.json'.format(type = assetData[0], item = st), 'w')
		fl.write(jsonFile)
		fl.close()
		
		apath = 'src/main/resources/assets/blocktools/textures/items/{type}_{item}.png'.format(item = st, type = assetData[0])
		args = 'convert {base} -channel RGB +level-colors ,{color} -channel RGBA {baseFull} +swap -layers merge {baseFull} -compose CopyOpacity -composite {output}'.format(
			base = 'base/{item}_head.png'.format(item = st),
			color = assetData[1],
			baseFull = 'base/{item}.png'.format(item = st),
			output = apath
		)
		
		subprocess.call(args, shell = True)
	
	for st in ['boots', 'leggings', 'chestplate', 'helmet']:
		jsonFile = '{{"parent": "item/handheld", "textures": {{"layer0": "blocktools:items/{type}_{item}"}}}}'.format(type = assetData[0], item = st)
		fl = open('src/main/resources/assets/blocktools/models/item/{type}_{item}.json'.format(type = assetData[0], item = st), 'w')
		fl.write(jsonFile)
		fl.close()
		
		apath = 'src/main/resources/assets/blocktools/textures/items/{type}_{item}.png'.format(item = st, type = assetData[0])
		args = 'convert -alpha background {base} +level-colors ,{color} {baseFull} -compose CopyOpacity -composite {output}'.format(
			base = 'base/{item}.png'.format(item = st),
			color = assetData[1],
			baseFull = 'base/{item}.png'.format(item = st),
			output = apath
		)
		
		subprocess.call(args, shell = True)
		
	apath = 'src/main/resources/assets/blocktools/textures/models/armor/armor_{type}_layer_1.png'.format(type = assetData[0])
	args = 'convert -alpha background {base} +level-colors ,{color} {baseFull} -compose CopyOpacity -composite {output}'.format(
		base = 'base/armor1.png',
		color = assetData[1],
		baseFull = 'base/armor1.png'.format(item = st),
		output = apath
	)
	
	subprocess.call(args, shell = True)
	
	apath = 'src/main/resources/assets/blocktools/textures/models/armor/armor_{type}_layer_2.png'.format(type = assetData[0])
	args = 'convert -alpha background {base} +level-colors ,{color} {baseFull} -compose CopyOpacity -composite {output}'.format(
		base = 'base/armor2.png',
		color = assetData[1],
		baseFull = 'base/armor2.png'.format(item = st),
		output = apath
	)
	
	subprocess.call(args, shell = True)

