
// 
// Copyright (C) 2017 DBot
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//      http://www.apache.org/licenses/LICENSE-2.0
//  
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 

package dbotthepony.minecraft.blocktools;

import dbotthepony.minecraft.blocktools.items.*;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.Item;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.item.ItemArmor;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

@Mod(modid = BlockTools.MOD_ID, name = BlockTools.MOD_NAME, version = "1.1.2")
public class BlockTools {
	public static final String MOD_ID =					"blocktools";
	public static final String MOD_NAME =				"Block Tools";
	
	public static HashMap<String, ToolMaterial> avaliableMaterials = new HashMap<String, ToolMaterial>();
	public static HashMap<String, ItemArmor.ArmorMaterial> avaliableArmorMaterials = new HashMap<String, ItemArmor.ArmorMaterial>();
	public static HashSet<Item> RegisteredItems = new HashSet<Item>();

	// 1 - for Iron properties
	public static ToolMaterial registerMaterial(String matID, int harvestLevel, int durability, double mult) {
		int ench = (int) (14 * mult);
		float speed = (float) (6.0 * mult);
		float damage = (float) (2.0 * mult);
		ToolMaterial newMat = EnumHelper.addToolMaterial(BlockTools.MOD_ID + "_" + matID, harvestLevel, durability, speed, damage, ench);
		avaliableMaterials.put(matID, newMat);
		return newMat;
	}

	public static ItemArmor.ArmorMaterial registerArmorMaterial(String matID, int durability, double mult, float tough, int ench) {
        ItemArmor.ArmorMaterial newMat = EnumHelper.addArmorMaterial(
            "blocktools_" + matID.toLowerCase(),
            "blocktools:armor_" + matID.toLowerCase(),
            (int) (durability * 9 * mult), new int[] {(int) (2 * mult), (int) (5 * mult), (int) (6 * mult), (int) (2 * mult)},
            ench,
            SoundEvents.ITEM_ARMOR_EQUIP_IRON,
            tough
        );

        avaliableArmorMaterials.put(matID, newMat);
		return newMat;
	}

	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		avaliableMaterials.put("IRON", ToolMaterial.IRON);
		avaliableMaterials.put("GOLD", ToolMaterial.GOLD);
		avaliableMaterials.put("DIAMOND", ToolMaterial.DIAMOND);
		
		registerMaterial("EMERALD", 3, 1800, 1.7);
		registerMaterial("COPPER", 2, 180, 0.9);
		registerMaterial("ZINC", 2, 170, 0.9);
		registerMaterial("LEAD", 2, 240, 0.9);
		registerMaterial("TIN", 2, 170, 0.9);
		registerMaterial("SILVER", 2, 230, 1.1);
		registerMaterial("PLATINUM", 3, 1700, 1.9);
		registerMaterial("BRONZE", 2, 330, 1);
		registerMaterial("INVAR", 2, 250, 1);
		registerMaterial("BRASS", 2, 280, 1.2);
		registerMaterial("STEEL", 2, 600, 1.2);

		/*
		LEATHER("leather", 5, new int[]{1, 2, 3, 1}, 15, SoundEvents.ITEM_ARMOR_EQUIP_LEATHER, 0.0F),
        CHAIN("chainmail", 15, new int[]{1, 4, 5, 2}, 12, SoundEvents.ITEM_ARMOR_EQUIP_CHAIN, 0.0F),
        IRON("iron", 15, new int[]{2, 5, 6, 2}, 9, SoundEvents.ITEM_ARMOR_EQUIP_IRON, 0.0F),
        GOLD("gold", 7, new int[]{1, 3, 5, 2}, 25, SoundEvents.ITEM_ARMOR_EQUIP_GOLD, 0.0F),
        DIAMOND("diamond", 33, new int[]{3, 6, 8, 3}, 10, SoundEvents.ITEM_ARMOR_EQUIP_DIAMOND, 2.0F);
        */

        avaliableArmorMaterials.put("IRON", EnumHelper.addArmorMaterial(
            "blocktools_iron",
            "blocktools:armor_iron",
            15 * 9, new int[] {2, 5, 6, 2},
            9,
             SoundEvents.ITEM_ARMOR_EQUIP_IRON,
            0F
        ));

        avaliableArmorMaterials.put("GOLD", EnumHelper.addArmorMaterial(
            "blocktools_gold",
            "blocktools:armor_gold",
            7 * 9, new int[] {1, 3, 5, 2},
            25,
             SoundEvents.ITEM_ARMOR_EQUIP_GOLD,
            0F
        ));

        avaliableArmorMaterials.put("DIAMOND", EnumHelper.addArmorMaterial(
            "blocktools_diamond",
            "blocktools:armor_diamond",
            33 * 9, new int[] {3, 6, 8, 3},
            10,
             SoundEvents.ITEM_ARMOR_EQUIP_GOLD,
            2F
        ));

        registerArmorMaterial("EMERALD", 50, 1.7, 4F, 15);
        registerArmorMaterial("COPPER", 13, 0.9, 0F, 11);
        registerArmorMaterial("ZINC", 10, 0.9, 0F, 9);
        registerArmorMaterial("LEAD", 13, 0.9, 0F, 10);
        registerArmorMaterial("TIN", 10, 0.9, 0F, 8);
        registerArmorMaterial("SILVER", 10, 1, 0F, 8);
        registerArmorMaterial("PLATINUM", 40, 1.9, 3F, 16);
        registerArmorMaterial("BRONZE", 17, 1, 0F, 8);
        registerArmorMaterial("INVAR", 16, 1, 0F, 9);
        registerArmorMaterial("BRASS", 16, 0.9, 0F, 13);
        registerArmorMaterial("STEEL", 20, 1.2, 0F, 12);

        for (Map.Entry<String, ToolMaterial> entry : avaliableMaterials.entrySet()) {
            String materialName = entry.getKey().toLowerCase();
            String genOreDict = "block" + materialName.substring(0, 1).toUpperCase() + materialName.substring(1);
            ToolMaterial material = entry.getValue();

            ItemBlockPickaxe pick = new ItemBlockPickaxe(material, materialName, genOreDict);
            RegisteredItems.add(pick);

            ItemBlockAxe axe = new ItemBlockAxe(material, materialName, genOreDict);
            RegisteredItems.add(axe);

            ItemBlockShovel shovel = new ItemBlockShovel(material, materialName, genOreDict);
            RegisteredItems.add(shovel);

            ItemBlockSword sword = new ItemBlockSword(material, materialName, genOreDict);
            RegisteredItems.add(sword);

            ItemBlockHoe hoe = new ItemBlockHoe(material, materialName, genOreDict);
            RegisteredItems.add(hoe);
        }

        for (Map.Entry<String, ItemArmor.ArmorMaterial> entry : avaliableArmorMaterials.entrySet()) {
            String materialName = entry.getKey().toLowerCase();
            String genOreDict = "block" + materialName.substring(0, 1).toUpperCase() + materialName.substring(1);
            ItemArmor.ArmorMaterial material = entry.getValue();

            ItemBlockArmor helmet = new ItemBlockArmor(material, materialName, EntityEquipmentSlot.HEAD, genOreDict);
            RegisteredItems.add(helmet);

            ItemBlockArmor chestplate = new ItemBlockArmor(material, materialName, EntityEquipmentSlot.CHEST, genOreDict);
            RegisteredItems.add(chestplate);

            ItemBlockArmor leggings = new ItemBlockArmor(material, materialName, EntityEquipmentSlot.LEGS, genOreDict);
            RegisteredItems.add(leggings);

            ItemBlockArmor boots = new ItemBlockArmor(material, materialName, EntityEquipmentSlot.FEET, genOreDict);
            RegisteredItems.add(boots);
        }
	}

    @Mod.EventBusSubscriber(modid = "blocktools")
	public static class Registration {
	    @SubscribeEvent
	    public static void registerBlocks(RegistryEvent.Register<Block> event) {

        }

        @SubscribeEvent
        public static void registerModels(ModelRegistryEvent event) {
            // for (Map.Entry<ItemStack, ModelResourceLocation> entry : BaseProxy.MODEL_LIST.entrySet()) {
                // ModelLoader.setCustomModelResourceLocation(entry.getKey().getItem(), entry.getKey().getItemDamage(), entry.getValue());
            // }

            for (Item entry : RegisteredItems) {
                ModelResourceLocation model = new ModelResourceLocation(entry.getRegistryName(), "inventory");
                ModelLoader.setCustomModelResourceLocation(entry, 0, model);
            }
        }

        @SubscribeEvent
	    public static void registerItems(RegistryEvent.Register<Item> event) {
            for (Item entry : RegisteredItems) {
                event.getRegistry().register(entry);
            }
        }
    }
}
