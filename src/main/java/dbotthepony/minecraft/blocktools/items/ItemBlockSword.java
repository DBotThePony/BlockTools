// 
// Copyright (C) 2017 DBot
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//      http://www.apache.org/licenses/LICENSE-2.0
//  
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
package dbotthepony.minecraft.blocktools.items;

import dbotthepony.minecraft.blocktools.BlockTools;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.oredict.ShapedOreRecipe;

public class ItemBlockSword extends ItemSword {
    private String __BlockToolsOreDict;
	
	public ItemBlockSword(ToolMaterial material, String name, String oreDict) {
		super(material);
        this.__BlockToolsOreDict = oreDict;
		
		this.setMaxDamage(material.getMaxUses() * 9);

        this.setRegistryName(name + "_sword");
        this.setUnlocalizedName("blocktools." + name + "_sword");
	}
	
	@Override
	public int getItemEnchantability() {
		return (int) (super.getItemEnchantability() * 1.2);
	}
	
	@Override
	public boolean getIsRepairable(ItemStack itemToRepair, ItemStack stack) {
		int[] valids = OreDictionary.getOreIDs(stack);
		
		for (int id : valids)
			if (OreDictionary.getOreName(id).equals(this.__BlockToolsOreDict))
				return true;
		
		return false;
	}
}
