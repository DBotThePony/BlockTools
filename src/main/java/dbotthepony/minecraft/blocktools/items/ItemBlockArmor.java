package dbotthepony.minecraft.blocktools.items;

import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;

import static net.minecraft.inventory.EntityEquipmentSlot.HEAD;

//
// Copyright (C) 2017 DBot
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//      http://www.apache.org/licenses/LICENSE-2.0
//  
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
public class ItemBlockArmor extends ItemArmor {
    private String __BlockToolsOreDict;

    public ItemBlockArmor(ArmorMaterial mat, String name, EntityEquipmentSlot equipSlot, String oreDict) {
        super(mat, 0, equipSlot);
        this.__BlockToolsOreDict = oreDict;

        String cName = null;

        switch (equipSlot) {
            case HEAD:
                cName = "helmet";
                break;
            case CHEST:
                cName = "chestplate";
                break;
            case LEGS:
                cName = "leggings";
                break;
            case FEET:
                cName = "boots";
                break;
        }

        this.setRegistryName(name + "_" + cName);
        this.setUnlocalizedName("blocktools." + name + "_" + cName);
    }

    @Override
    public int getItemEnchantability() {
        return (int) (super.getItemEnchantability() * 1.2);
    }

    @Override
    public boolean getIsRepairable(ItemStack itemToRepair, ItemStack stack) {
        int[] valids = OreDictionary.getOreIDs(stack);

        for (int id : valids)
            if (OreDictionary.getOreName(id).equals(this.__BlockToolsOreDict))
                return true;

        return false;
    }
}
