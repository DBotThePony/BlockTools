
// 
// Copyright (C) 2017 DBot
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//      http://www.apache.org/licenses/LICENSE-2.0
//  
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 

package dbotthepony.minecraft.blocktools.items;

import net.minecraft.item.ItemSpade;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;

public class ItemBlockShovel extends ItemSpade {
    private String __BlockToolsOreDict;

	public ItemBlockShovel(ToolMaterial mat, String name, String oreDict) {
		super(mat);
		this.toolMaterial = mat;
		this.damageVsEntity = 4;
		this.attackSpeed = -3.0F;

        this.__BlockToolsOreDict = oreDict;

        this.setMaxDamage(mat.getMaxUses() * 9);
        this.setRegistryName(name + "_shovel");
        this.setUnlocalizedName("blocktools." + name + "_shovel");
	}

    @Override
    public int getItemEnchantability() {
        return (int) (super.getItemEnchantability() * 1.2);
    }

    @Override
    public boolean getIsRepairable(ItemStack itemToRepair, ItemStack stack) {
        int[] valids = OreDictionary.getOreIDs(stack);

        for (int id : valids)
            if (OreDictionary.getOreName(id).equals(this.__BlockToolsOreDict))
                return true;

        return false;
    }
}
