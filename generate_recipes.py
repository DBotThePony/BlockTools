
# 
# Copyright (C) 2017 DBot
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#      http://www.apache.org/licenses/LICENSE-2.0
#  
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 

# IMAGE MAGICK IS REQUIRED!

import subprocess

assets = [
	['iron', 'White'],
	['gold', 'Gold'],
	['diamond', '#6495ED'],
	['emerald', 'Lime'],
	['copper', 'rgb(184,115,51)'],
	['zinc', '#778899'],
	['lead', '#D0D0D0'],
	['tin', '#D3D3D3'],
	['silver', '#C0C0C0'],
	['platinum', '#B0C4DE'],
	['bronze', '#CD853F'],
	['invar', '#F5DEB3'],
	['brass', '#808080'],
	['steel', '#808080'],
]

def easyWrite(path, content):
	fl = open(path, 'w')
	fl.write(content)
	fl.close()

jsonAxe = '''
{{
	"type": "minecraft:crafting_shaped",
	"pattern": ["BB", "BS", " S"],
	"mirrored": true,
	"key": {{
		"S": {{"ore": "stickWood", "type": "forge:ore_dict"}},
		"B": {{"ore": "{oreDict}", "type": "forge:ore_dict"}}
	}},
	"result": {{
		"item": "blocktools:{type}_axe",
		"data": 0
	}}
}}
'''

jsonPickaxe = '''
{{
	"type": "minecraft:crafting_shaped",
	"pattern": ["BBB", " S ", " S "],
	"mirrored": false,
	"key": {{
		"S": {{"ore": "stickWood", "type": "forge:ore_dict"}},
		"B": {{"ore": "{oreDict}", "type": "forge:ore_dict"}}
	}},
	"result": {{
		"item": "blocktools:{type}_pickaxe",
		"data": 0
	}}
}}
'''

jsonSword = '''
{{
	"type": "minecraft:crafting_shaped",
	"pattern": ["B", "B", "S"],
	"mirrored": false,
	"key": {{
		"S": {{"ore": "stickWood", "type": "forge:ore_dict"}},
		"B": {{"ore": "{oreDict}", "type": "forge:ore_dict"}}
	}},
	"result": {{
		"item": "blocktools:{type}_sword",
		"data": 0
	}}
}}
'''

jsonShovel = '''
{{
	"type": "minecraft:crafting_shaped",
	"pattern": ["B", "S", "S"],
	"mirrored": false,
	"key": {{
		"S": {{"ore": "stickWood", "type": "forge:ore_dict"}},
		"B": {{"ore": "{oreDict}", "type": "forge:ore_dict"}}
	}},
	"result": {{
		"item": "blocktools:{type}_shovel",
		"data": 0
	}}
}}
'''

jsonHoe = '''
{{
	"type": "minecraft:crafting_shaped",
	"pattern": ["BB", " S", " S"],
	"mirrored": true,
	"key": {{
		"S": {{"ore": "stickWood", "type": "forge:ore_dict"}},
		"B": {{"ore": "{oreDict}", "type": "forge:ore_dict"}}
	}},
	"result": {{
		"item": "blocktools:{type}_shovel",
		"data": 0
	}}
}}
'''

for assetData in assets:
	cItem = 'axe'
	oreDict = 'block' + assetData[0][0].upper() + assetData[0][1:]
	
	jsonFile = jsonAxe.format(type = assetData[0], oreDict = oreDict)
	easyWrite('src/main/resources/assets/blocktools/recipes/{type}_{item}.json'.format(type = assetData[0], item = cItem), jsonFile)
	
	cItem = 'pickaxe'
	jsonFile = jsonPickaxe.format(type = assetData[0], oreDict = oreDict)
	easyWrite('src/main/resources/assets/blocktools/recipes/{type}_{item}.json'.format(type = assetData[0], item = cItem), jsonFile)
	
	cItem = 'sword'
	jsonFile = jsonSword.format(type = assetData[0], oreDict = oreDict)
	easyWrite('src/main/resources/assets/blocktools/recipes/{type}_{item}.json'.format(type = assetData[0], item = cItem), jsonFile)
	
	cItem = 'shovel'
	jsonFile = jsonShovel.format(type = assetData[0], oreDict = oreDict)
	easyWrite('src/main/resources/assets/blocktools/recipes/{type}_{item}.json'.format(type = assetData[0], item = cItem), jsonFile)
	
	cItem = 'hoe'
	jsonFile = jsonHoe.format(type = assetData[0], oreDict = oreDict)
	easyWrite('src/main/resources/assets/blocktools/recipes/{type}_{item}.json'.format(type = assetData[0], item = cItem), jsonFile)
	
	
	


